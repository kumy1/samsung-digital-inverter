/**
 * Copyright (c) 2010-2018 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */

package org.openhab.binding.samsungdigitalinverter.handler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Base64;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.lang.StringUtils;
import org.eclipse.smarthome.core.library.types.DateTimeType;
import org.eclipse.smarthome.core.library.types.DecimalType;
import org.eclipse.smarthome.core.library.types.PointType;
import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.thing.Channel;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.RefreshType;
import org.eclipse.smarthome.core.types.State;
import org.eclipse.smarthome.core.types.UnDefType;
import org.openhab.binding.samsungdigitalinverter.SamsungDigitalInverterConstants;
import org.openhab.binding.samsungdigitalinverter.internal.SamsungDigitalInverterConfiguration;
import org.openhab.binding.samsungdigitalinverter.json.SamsungDigitalInverterJsonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

/**
 *
 * The {@link SamsungDigitalInverterHandler} class is the core class for the Samsung Digital Inverter
 *
 * @author Jan Grønlien - Initial contribution
 */

public class SamsungDigitalInverterHandler extends BaseThingHandler {

    private Logger logger = LoggerFactory.getLogger(SamsungDigitalInverterHandler.class);

    private SamsungDigitalInverterConfiguration config;

    private static final int DEFAULT_REFRESH_PERIOD = 60;

    private static final long POWER_USAGE_REFRESH = 30 * 60 * 1000;

    private ScheduledFuture<?> refreshJob;

    private SamsungDigitalInverterJsonResponse sdiResponse;

    private Gson gson;

    private HostnameVerifier allHostsValid;

    private KeyManager[] keyManagers;

    private TrustManager[] trustManagers;

    private SSLContext sslContext;

    private static final String powerUsageSQL = "select date, power_usage, running_time from power_usage_table where date >= (select max(date) from power_usage_table where date < (select max(date) from power_usage_table)) order by date;";

    private long lastUpdateTimeMillis;

    private PowerUsage currentPowerUsage;

    public SamsungDigitalInverterHandler(Thing thing) {
        super(thing);
        gson = new Gson();

    }

    @Override
    public void initialize() {
        logger.debug("Initializing Samsung Digital Inverter handler.");

        config = getConfigAs(SamsungDigitalInverterConfiguration.class);
        logger.debug("config bearer = (omitted from logging)");
        logger.debug("config ip = {}", config.ip);
        logger.debug("config port = {}", config.port);
        logger.debug("config refresh = {}", config.refresh);
        logger.debug("config keystore = {}", config.keystore);
        logger.debug("config keystore secret = (omitted from logging)");
        lastUpdateTimeMillis = 0L;

        String errorMsg = null;

        try {
            allHostsValid = createrHostnameVerifier();
            keyManagers = createKeyManagers(config.keystore, config.keystore_secret);
            trustManagers = createTrustAll();

            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(keyManagers, trustManagers, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            Class.forName("org.sqlite.JDBC");

        } catch (Exception e) {
            errorMsg = e.getMessage();
        }

        if (errorMsg == null) {
            updateStatus(ThingStatus.ONLINE);
            startAutomaticRefresh();
        } else {
            logger.error(errorMsg);
            updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR, errorMsg);
        }
    }

    /**
     * Start the job refreshing the Samsung Digital Inverter data
     */
    private void startAutomaticRefresh() {
        if (refreshJob == null || refreshJob.isCancelled()) {
            Runnable runnable = () -> {
                try {
                    logger.debug("Starting Refresh");
                    // Call Samsung Digital Inverter to get data
                    sdiResponse = getSamsungDigitalInverterData();

                    // downloading power usage database is very consuming for the Digital Inverter, so the usage.db are
                    // downloaded twice per hour.
                    if ((System.currentTimeMillis() - lastUpdateTimeMillis) > POWER_USAGE_REFRESH) {
                        logger.debug("Updating power usage, last update was on {}", lastUpdateTimeMillis);
                        lastUpdateTimeMillis = System.currentTimeMillis();
                        currentPowerUsage = getPowerUsage();
                    }

                    sdiResponse.setPowerUsage(currentPowerUsage);
                    sdiResponse.setPowerUsageDifference((currentPowerUsage.getCurrentPowerUsage()
                            .subtract(currentPowerUsage.getPreviousPowerUsage()))
                                    .divide(currentPowerUsage.getDuration()));

                    logger.debug("Power Usage {}, usage since last update {}", currentPowerUsage.toString(),
                            sdiResponse.getPowerUsageDifference());

                    // Update all channels
                    for (Channel channel : getThing().getChannels()) {
                        updateChannel(channel.getUID().getId(), sdiResponse);
                    }
                } catch (Exception e) {
                    logger.error("Exception occurred during execution: {}", e.getMessage(), e);
                }
            };

            SamsungDigitalInverterConfiguration config = getConfigAs(SamsungDigitalInverterConfiguration.class);
            int delay = (config.refresh != null) ? config.refresh.intValue() : DEFAULT_REFRESH_PERIOD;
            refreshJob = scheduler.scheduleWithFixedDelay(runnable, 0, delay, TimeUnit.SECONDS);
        }
    }

    private PowerUsage getPowerUsage() {
        PowerUsage pu = null;
        byte[] db = getUsageDB();

        File usageDB = null;
        Connection conn = null;
        try {
            usageDB = File.createTempFile("sdi", "db");
            usageDB.deleteOnExit();
            FileOutputStream os = new FileOutputStream(usageDB);
            os.write(db);
            os.close();
            logger.debug("Power Usage DB saved as {}", usageDB.getAbsolutePath());

            conn = DriverManager.getConnection("jdbc:sqlite:" + usageDB.getAbsolutePath());
            PreparedStatement ps = conn.prepareStatement(powerUsageSQL);
            ResultSet rs = ps.executeQuery();

            pu = new PowerUsage();
            // Two rows are returned from the table, first row is the previous save of power and running time
            if (rs.next()) {
                pu.setPreviousDate(rs.getString(1));
                String str = rs.getString(2);
                pu.setPreviousPowerUsage(str);
                str = rs.getString(3);
                pu.setPreviousRunningTime(str);
                logger.debug("date {}, power_usage {}, running_time {}", rs.getString(1), rs.getBigDecimal(2),
                        rs.getBigDecimal(3));
            }
            // Second row contans the last saved data
            if (rs.next()) {
                pu.setCurrentDate(rs.getString(1));
                pu.setCurrentPowerUsage(rs.getString(2));
                pu.setCurrentRunningTime(rs.getString(3));
                logger.debug("date {}, power_usage {}, running_time {}", rs.getString(1), rs.getBigDecimal(2),
                        rs.getBigDecimal(3));
            } else {
                pu.setPreviousDate(rs.getString(1));
                pu.setPreviousPowerUsage(rs.getBigDecimal(2));
                pu.setPreviousRunningTime(rs.getBigDecimal(3));
                logger.debug("single row detected in power usage table");
            }
            rs.close();
            ps.close();

        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        } catch (SQLException e) {
            logger.error(e.getMessage());
        } catch (ParseException e) {
            logger.error(e.getMessage());
        } finally

        {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
            if (usageDB != null) {
                usageDB.delete();
            }
        }

        return (pu);

    }

    @Override
    public void dispose() {
        logger.debug("Disposing the Samsung Digital Inverter handler.");

        if (refreshJob != null && !refreshJob.isCancelled()) {
            refreshJob.cancel(true);
            refreshJob = null;
        }
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        if (command instanceof RefreshType) {
            updateChannel(channelUID.getId(), sdiResponse);
        } else {
            String channel = channelUID.getId();
            JsonCommand sdiCommand = null;
            switch (channel) {
                case SamsungDigitalInverterConstants.SETPOINT_TEMPERATURE:
                    sdiCommand = SamsungDigitalInverterCommands.createDesiredTemperatureCommand(command);
                    break;
                case SamsungDigitalInverterConstants.POWER:
                    sdiCommand = SamsungDigitalInverterCommands.createPowerCommand(command);
                    break;
                case SamsungDigitalInverterConstants.WIND_DIRECTION:
                    sdiCommand = SamsungDigitalInverterCommands.createWindDirectionCommand(command);
                    break;
                case SamsungDigitalInverterConstants.WIND_SPEED:
                    sdiCommand = SamsungDigitalInverterCommands.createDesiredWindSpeedCommand(command);
                    break;
                case SamsungDigitalInverterConstants.MAX_WIND_SPEED:
                    sdiCommand = SamsungDigitalInverterCommands.createMaxWindSpeedCommand(command);
                    break;
                case SamsungDigitalInverterConstants.OPERATING_MODE:
                    sdiCommand = SamsungDigitalInverterCommands.createSetOperatingModeCommand(command);
                    break;
                case SamsungDigitalInverterConstants.COMODE:
                    sdiCommand = SamsungDigitalInverterCommands.createSetComodeCommand(command);
                    break;
                case SamsungDigitalInverterConstants.BEEP:
                    sdiCommand = SamsungDigitalInverterCommands.createSetBeepCommand(command);
                    break;
                case SamsungDigitalInverterConstants.AUTOCLEAN:
                    sdiCommand = SamsungDigitalInverterCommands.createSetAutoCleanCommand(command);
                    break;
                case SamsungDigitalInverterConstants.RESET_FILTER_CLEAN_ALARM:
                    sdiCommand = SamsungDigitalInverterCommands.createResetFilterCleanAlarm(command);
                    break;
            }
            logger.debug("The Samsung Digital Inverter handle command {} -> {}", channelUID, command);
            if (sdiCommand != null) {
                try {
                    logger.debug("To execute command: {} {}", sdiCommand.getPath(), sdiCommand.getJson());
                    updateService(sdiCommand);
                } catch (IOException e) {
                    logger.error(e.getMessage());
                }
            }
        }
    }

    /**
     * @param sdiCommand
     * @throws IOException
     */
    private void updateService(JsonCommand sdiCommand) throws IOException {
        URL url = createURL(sdiCommand.getPath());

        logger.debug("URL = {}", url);

        HttpsURLConnection con = openConnection(url);
        con.setRequestMethod("PUT");
        con.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(sdiCommand.getJson());
        wr.flush();

        int httpResult = con.getResponseCode();
        logger.debug("updateService {} with {} returned {}", sdiCommand.getPath(), sdiCommand.getJson(), httpResult);

    }

    /**
     *
     * Update with response from Samsung Digital Inverter
     *
     * @param channelId   channel to update
     * @param sdiResponse Class representing the json response from the Samsung Digital Inverter
     */
    private void updateChannel(String channelId, SamsungDigitalInverterJsonResponse sdiResponse) {
        if (isLinked(channelId)) {
            Object value;
            try {
                value = getValue(channelId, sdiResponse);
            } catch (Exception e) {
                logger.debug("Device doesn't provide {} measurement", channelId.toUpperCase());
                return;
            }

            State state = null;
            if (value == null) {
                state = UnDefType.UNDEF;
            } else if (value instanceof PointType) {
                state = (PointType) value;
            } else if (value instanceof ZonedDateTime) {
                state = new DateTimeType((ZonedDateTime) value);
            } else if (value instanceof QuantityType<?>) {
                state = (QuantityType<?>) value;
            } else if (value instanceof BigDecimal) {
                state = new DecimalType((BigDecimal) value);
            } else if (value instanceof Integer) {
                state = new DecimalType(BigDecimal.valueOf(((Integer) value).longValue()));
            } else if (value instanceof String) {
                state = new StringType(value.toString());
            } else if (value instanceof Float) {
                state = new DecimalType((Float) value);
            } else {
                logger.warn("Update channel {}: Unsupported value type {}", channelId,
                        value.getClass().getSimpleName());
            }
            logger.debug("Update channel {} with state {} ({})", channelId, (state == null) ? "null" : state.toString(),
                    (value == null) ? "null" : value.getClass().getSimpleName());

            // Update the channel
            if (state != null) {
                updateState(channelId, state);
            }
        }
    }

    /**
     *
     * Make Call to Samsung Digital Inverter
     * Data is returned as json
     *
     */
    private SamsungDigitalInverterJsonResponse getSamsungDigitalInverterData() {
        SamsungDigitalInverterJsonResponse result = null;
        String errorMsg = null;

        try {
            URL url = createURL(SamsungDigitalInverterConstants.DEVICES);
            logger.debug("URL = {}", url);

            URLConnection con = openConnection(url);

            String response = convertStreamToString(con.getInputStream());
            logger.debug("Response: {}", response);

            // Map the JSON response to an object
            result = gson.fromJson(response, SamsungDigitalInverterJsonResponse.class);

            if (result.getDevices() != null) {
                updateStatus(ThingStatus.ONLINE, ThingStatusDetail.NONE, null);
            } else {
                errorMsg = "missing data sub-object";
                logger.warn("Invalid response: {}", errorMsg);
            }

            // Download usage.db
            // usageDB = getUsageDB();
        } catch (Exception e) {
            logger.error("{}", e.getMessage());
            updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR, e.getMessage());
        }
        return (result);
    }

    private byte[] getUsageDB() {
        byte[] ret = null;

        try {
            URL url = createURL(SamsungDigitalInverterConstants.USAGE_DB);
            logger.debug("URL = {}", url);
            URLConnection con = openConnection(url);

            String response = convertStreamToString(con.getInputStream());
            ret = Base64.getDecoder().decode(response);
        } catch (MalformedURLException e) {
            logger.error("{}", e.getMessage());
        } catch (IOException e) {
            logger.error("{}", e.getMessage());
        }

        return (ret);
    }

    /**
     *
     * Get value from json response, convert values to wanted type.
     *
     * @param channelId
     * @param data
     * @return Object with data from response
     * @throws Exception
     */
    public static Object getValue(String channelId, SamsungDigitalInverterJsonResponse data) throws Exception {
        String[] fields = StringUtils.split(channelId, "#");
        Object value;

        if (data != null) {
            switch (fields[0]) {
                case SamsungDigitalInverterConstants.TEMPERATURE_CURRENT:
                    return data.getDevices().get(0).getTemperatures().get(0).getCurrent();
                case SamsungDigitalInverterConstants.SETPOINT_TEMPERATURE:
                    return data.getDevices().get(0).getTemperatures().get(0).getDesired();
                case SamsungDigitalInverterConstants.OUTDOOR_TEMPERATURE:
                    return new BigDecimal(data.getDevices().get(0).getMode().getOptions().get(4).substring(12));
                case SamsungDigitalInverterConstants.POWER:
                    return "On".equals(data.getDevices().get(0).getOperation().getPower())
                            ? SamsungDigitalInverterConstants.ON
                            : SamsungDigitalInverterConstants.OFF;
                case SamsungDigitalInverterConstants.WIND_SPEED:
                    return data.getDevices().get(0).getWind().getSpeedLevel().toString();
                case SamsungDigitalInverterConstants.MAX_WIND_SPEED:
                    return data.getDevices().get(0).getWind().getMaxSpeedLevel().toString();
                case SamsungDigitalInverterConstants.WIND_DIRECTION:
                    return data.getDevices().get(0).getWind().getDirection();
                case SamsungDigitalInverterConstants.ALARM:
                    value = data.getDevices().get(0).getAlarms().get(0).getCode();
                    if ("FilterAlarm_OFF".equals(value)) {
                        value = "";
                    } else if ("FilterAlarm".equals(value)) {
                        value = "Please Clean Filter";
                    }
                    return value;
                case SamsungDigitalInverterConstants.OPERATING_MODE:
                    return data.getDevices().get(0).getMode().getModes().get(0);
                case SamsungDigitalInverterConstants.OPTIONS:
                    value = data.getDevices().get(0).getMode().getOptions();
                    return value;
                case SamsungDigitalInverterConstants.COMODE:
                    value = data.getDevices().get(0).getMode().getOptions().get(0);
                    return value;
                case SamsungDigitalInverterConstants.FILTER_ALARMTIME:
                    value = data.getDevices().get(0).getMode().getOptions().get(11).substring(16);
                    value = new BigDecimal((String) value);
                    return value;
                case SamsungDigitalInverterConstants.FILTERTIME:
                    value = data.getDevices().get(0).getMode().getOptions().get(8).substring(11);
                    value = Float.valueOf((Float.valueOf((String) value) / 10));
                    return value;
                case SamsungDigitalInverterConstants.AUTOCLEAN:
                    return "Autoclean_On".equals(data.getDevices().get(0).getMode().getOptions().get(2))
                            ? SamsungDigitalInverterConstants.ON
                            : SamsungDigitalInverterConstants.OFF;
                case SamsungDigitalInverterConstants.BEEP:
                    return "Volume_100".equals(data.getDevices().get(0).getMode().getOptions().get(13))
                            ? SamsungDigitalInverterConstants.ON
                            : SamsungDigitalInverterConstants.OFF;
                case SamsungDigitalInverterConstants.RESET_FILTER_CLEAN_ALARM:
                    return "FilterCleanAlarm_0".equals(data.getDevices().get(0).getMode().getOptions().get(3))
                            ? SamsungDigitalInverterConstants.OFF
                            : SamsungDigitalInverterConstants.ON;
                case SamsungDigitalInverterConstants.POWER_USAGE:
                    return data.getPowerUsage().getCurrentPowerUsage();
                case SamsungDigitalInverterConstants.RUNNING_TIME:
                    return data.getPowerUsage().getCurrentRunningTime();
                case SamsungDigitalInverterConstants.POWER_USAGE_DIFFERENCE:
                    return data.getPowerUsageDifference();
            }
        } else {
            throw new NullPointerException("data can't be null for channelID=" + channelId);
        }

        return null;
    }

    private TrustManager[] createTrustAll() {
        TrustManager[] ret = new TrustManager[] { new X509TrustManager() {
            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            @Override
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };

        return (ret);

    }

    private KeyManager[] createKeyManagers(String keystoreFilePath, String keyStorePassword) throws KeyStoreException,
            NoSuchAlgorithmException, CertificateException, IOException, UnrecoverableKeyException {

        InputStream keyStoreInput = new FileInputStream(keystoreFilePath);
        KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(keyStoreInput, keyStorePassword.toCharArray());
        // keystore.
        KeyManagerFactory kmfactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmfactory.init(keyStore, keyStorePassword.toCharArray());
        return kmfactory.getKeyManagers();

    }

    private HostnameVerifier createrHostnameVerifier() {
        return (new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });
    }

    private URL createURL(String path) throws MalformedURLException {
        return (new URL("https://" + config.ip + ":" + config.port + "/" + path));
    }

    private HttpsURLConnection openConnection(URL url) throws IOException {
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
        con.addRequestProperty("Content-Type", "application/json");
        con.addRequestProperty("Authorization", "Bearer " + config.bearer);
        return (con);
    }

    private String convertStreamToString(InputStream inputStream) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

        String line = null;
        boolean isFirst = true;

        while ((line = br.readLine()) != null) {
            if (isFirst) {
                sb.append(line);
            } else {
                sb.append("\n").append(line);
            }
            isFirst = false;
        }

        return sb.toString();
    }
}
